FROM node:14.14.0-buster AS base
RUN npm install -g @angular/cli
RUN npm install --save-dev @angular-devkit/build-angular


FROM base AS build
WORKDIR /app
COPY . .
RUN npm install
RUN ng build --prod


FROM node:14.14.0-buster-slim AS final
WORKDIR /app
COPY --from=build /app/dist /app/dist
RUN npm install -g serve
EXPOSE 5000
CMD serve /app/dist/frontend


FROM base AS debug
ENV KUBECONFIG=/kube/config
RUN apt update && apt install curl -y

RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.19.2/bin/linux/amd64/kubectl
RUN chmod +x ./kubectl
RUN mv ./kubectl /usr/bin/kubectl

RUN curl -s https://packagecloud.io/install/repositories/datawireio/telepresence/script.deb.sh | bash
RUN apt update && apt-get -y install --no-install-recommends apt-utils git procps lsb-release sudo iptables curl telepresence
RUN apt autoremove -y && apt clean -y && rm -rf /var/lib/apt/lists/*
EXPOSE 4200
