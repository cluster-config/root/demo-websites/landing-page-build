import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PointerModelDemoComponent } from './pointer-model-demo.component';

describe('PointerModelDemoComponent', () => {
  let component: PointerModelDemoComponent;
  let fixture: ComponentFixture<PointerModelDemoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PointerModelDemoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PointerModelDemoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
