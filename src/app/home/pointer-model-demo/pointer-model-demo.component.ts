import { Component, OnInit } from '@angular/core';
import calc_x_y_delta from "src/app/home/pointer-model-demo/calc_x_y_delta.js"

function calc_mean(coords: number[], new_coord: number, limit: number) {
  let new_pointer_x = Math.floor(Math.min(limit - 4, Math.max(0, new_coord)));

  const queue_size = 2;

  coords.push(new_pointer_x)
  if (coords.length >= queue_size) coords.shift()

  let weighted_coord_sum = 0;
  let weight_sum = 0;
  for (let i = 0; i < coords.length; i++) {
    let weight = 1 / (Math.pow(i, 10) + 1);
    let coord = coords[i];
    weighted_coord_sum += coord * weight;
    weight_sum += weight;
  }
  return weighted_coord_sum / weight_sum;
}

@Component({
  selector: 'app-pointer-model-demo',
  templateUrl: './pointer-model-demo.component.html',
  styleUrls: ['./pointer-model-demo.component.scss'],
})
export class PointerModelDemoComponent implements OnInit {
  pointer_x: number;
  pointer_y: number;
  left_clicked: boolean;
  color: string;
  window: Window & typeof globalThis;
  pointer_xs: number[];
  pointer_ys: number[];

  change_pointer_pos(pointer_model: PointerModelDemoComponent, new_coords: any, left_clicked: boolean) {
    let new_pointer_x = calc_mean(pointer_model.pointer_xs, new_coords.x, window.innerWidth);
    let new_pointer_y = calc_mean(pointer_model.pointer_ys, new_coords.y, window.innerHeight);

    if ((Math.sqrt(Math.pow(new_pointer_x - pointer_model.pointer_x, 2) + Math.pow(new_pointer_y - pointer_model.pointer_y, 2))) > 4) {
      pointer_model.pointer_x = new_pointer_x
      pointer_model.pointer_y = new_pointer_y
    }


    if (left_clicked) {
      pointer_model.color = "#00FF00";
      let elem = document.elementFromPoint(pointer_model.pointer_x, pointer_model.pointer_y) as HTMLElement;
      if (elem) {
        elem.click();
        elem.focus();
      }
    }
    else
      pointer_model.color = "#FF0000";
  }

  constructor() {
    this.pointer_x = window.innerWidth * 0.5;
    this.pointer_y = window.innerHeight * 0.5;
    this.pointer_xs = new Array();
    this.pointer_ys = new Array();
    this.left_clicked = false;
    this.color = "#FF0000";
    this.window = window;
  }

  async ngOnInit(): Promise<void> {
    calc_x_y_delta(this, this.change_pointer_pos)
  }
}
