export default function calc_x_y_delta(pointer_model, change_pointer_pos) {
  const videoElement = document.getElementsByClassName('input_video')[0];
  const canvasElement = document.getElementsByClassName('output_canvas')[0];
  const canvasCtx = canvasElement.getContext('2d');
  function onResults(results) {
    canvasCtx.save();
    canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    canvasCtx.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
    if (results.multiHandLandmarks) {
      let thumb = results.multiHandLandmarks[0][4];
      let index = results.multiHandLandmarks[0][8];
      let mid_knuckle = results.multiHandLandmarks[0][9];
      let bottom_palm = results.multiHandLandmarks[0][0];
      if (thumb && index && mid_knuckle && bottom_palm) {
        drawLandmarks(canvasCtx, [thumb], { color: '#FF0000', lineWidth: 1 });
        drawLandmarks(canvasCtx, [index], { color: '#00FF00', lineWidth: 1 });
        drawLandmarks(canvasCtx, [mid_knuckle], { color: '#0000FF', lineWidth: 1 });
        drawLandmarks(canvasCtx, [bottom_palm], { color: '#FFFFFF', lineWidth: 1 });

        let measure_against = 0.5 * Math.sqrt(Math.pow(mid_knuckle.x - bottom_palm.x, 2) + Math.pow(mid_knuckle.y - bottom_palm.y, 2));
        let left_click_measurement = Math.sqrt(Math.pow(mid_knuckle.x - index.x, 2) + Math.pow(mid_knuckle.y - index.y, 2));
        let left_clicked = left_click_measurement < measure_against;

        let zoom_into_middle_by = 0.3;
        [thumb, index, mid_knuckle, bottom_palm].forEach((point) => point.x = window.innerWidth * (point.x * (1 + 2 * zoom_into_middle_by) - zoom_into_middle_by));
        [thumb, index, mid_knuckle, bottom_palm].forEach((point) => point.y = window.innerHeight * (point.y * (1 + 2 * zoom_into_middle_by) - zoom_into_middle_by));

        change_pointer_pos(pointer_model, mid_knuckle, left_clicked)
      }
    } else {
      pointer_model.color = "#FF0000";
    }
    canvasCtx.restore();
  }

  const hands = new Hands({
    locateFile: (file) => {
      return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
    }
  });
  hands.setOptions({
    selfieMode: true,
    maxNumHands: 1,
    minDetectionConfidence: 0.8,
    minTrackingConfidence: 0.5
  });
  hands.onResults(onResults);

  const camera = new Camera(videoElement, {
    onFrame: async () => {
      await hands.send({ image: videoElement });
    },
    width: 640,
    height: 360
  });
  camera.start();
}
