import { Component, OnInit } from '@angular/core';
import { GoBackService } from '../go-back.service';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.scss']
})
export class PageNotFoundComponent implements OnInit {

  constructor(private goBackService: GoBackService) {
  }

  ngOnInit(): void {
  }

  goBack(): string {
    console.log(this.goBackService.previousUrl)
    return this.goBackService.previousUrl
  }
}
