import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { GoBackService } from './go-back.service';
import { ShowcaseFormComponent } from './home/showcase-form/showcase-form.component';
import { PointerModelDemoComponent } from './home/pointer-model-demo/pointer-model-demo.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    AboutComponent,
    PageNotFoundComponent,
    ShowcaseFormComponent,
    PointerModelDemoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [GoBackService],
  bootstrap: [AppComponent]
})
export class AppModule { }
