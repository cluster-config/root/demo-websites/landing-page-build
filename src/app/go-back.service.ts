import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GoBackService {
  private _previousUrl: string;
  private _currentUrl: string;

  constructor(private router: Router) {
    this._currentUrl = this.router.url;
    router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this._previousUrl = this._currentUrl;
        this._currentUrl = event.url;
      };
    });
  }

  get previousUrl() {
    return this._previousUrl;
  }
}
